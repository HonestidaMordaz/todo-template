(function () {
	'use strict'

	var dom = document;

	if (supportsTemplate()) {
		var btn = dom.querySelector('.btn')

		btn.addEventListener('click', onClickBtn)
	}

	function supportsTemplate() {
	  return 'content' in dom.createElement('template');
	}

	function onClickBtn (evt) {
		evt.preventDefault()

		var t = dom.querySelector('.template')
		var ul = t.content.querySelector('ul')
		var txt = dom.querySelector('.txt')
		var li = dom.createElement('li')

		li.textContent = txt.value.trim()

		if (li.textContent != '') {
			ul.appendChild(li)

			txt.value = ''

			var clone = dom.importNode(ul, true)

			dom.querySelector('.container ul').innerHTML = clone.innerHTML

			console.log(clone)
		}
	}
})()